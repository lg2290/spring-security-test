package com.example.demo.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

	@RequestMapping("/")
	public String loggedUser(Principal user) {
		return user.getName();
	}
}
